import { File } from '@prisma/client';

export interface CreateFileDto {
  buffer: Buffer;
  objectId: string;
  mimeType: string;
}

export interface CreateFileResponse {
  status: string;
  data: File;
}
