import { Controller, Logger } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { catchError, firstValueFrom } from 'rxjs';
import { CreateFileDto } from './dto/create-file.dto';
import { SearchFile } from './dto/search-file.dto';
import { LocalStorageService } from './local-storage/local-storage.service';

@Controller('')
export class StorageController {
  private logger: Logger;
  constructor(private readonly localStorageService: LocalStorageService) {
    this.logger = new Logger('StorageController');
  }

  @MessagePattern('upload-file')
  async uploadFileTCP(@Payload() payload: CreateFileDto) {
    this.logger.log('Creating file metadata by objectId: ' + payload.objectId);
    const uuid = await this.localStorageService.createFileObjectMetadata(
      payload.objectId,
    );

    this.logger.log('Produce TOPIC: create-meta objectId ' + uuid);
    this.logger.debug({
      uuid,
      objectId: payload.objectId,
      mimeType: payload.mimeType,
    });

    firstValueFrom(
      this.localStorageService
        .produceSaveFile(
          uuid,
          payload.objectId,
          payload.buffer,
          payload.mimeType,
        )
        .pipe(
          catchError((e) => {
            this.logger.error(e);
            throw e;
          }),
        ),
    ).then((res) => {
      this.logger.log('Produced TOPIC: create-meta objectId ' + uuid);
      this.logger.debug(res);
    });

    return {
      status: 'ok',
      data: {
        objectId: payload.objectId,
      },
    };
  }

  @MessagePattern('get-file')
  async getFile(@Payload() payload: SearchFile) {
    const objectFile = await this.localStorageService.getObjectByObjectID(
      payload.objectId,
    );
    if (objectFile) {
      return objectFile;
    }
    return {
      error: 'file not found',
    };
  }

  @MessagePattern('delete-file')
  async deleteFile(@Payload() payload: SearchFile) {
    const objectFile = await this.localStorageService.getObjectByObjectID(
      payload.objectId,
    );
    if (!objectFile) {
      return {
        error: 'invalid id',
      };
    }

    this.localStorageService.produceDeleteFile(objectFile.objectID);
    return {
      status: 'ok',
      data: {
        objectId: payload.objectId,
      },
    };
  }

  @MessagePattern('update-file')
  replaceFile(@Payload() payload: CreateFileDto) {
    this.localStorageService.produceDeleteFile(payload.objectId);

    return {
      status: 'ok',
      data: {
        objectId: payload.objectId,
      },
    };
  }
}
