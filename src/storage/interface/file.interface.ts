export interface File {
  path: string;
  id: string;
  contentType: string;
  size: string;
  uploadby: string;
}
