import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { PrismaModule } from 'src/prisma/prisma.module';
import { LocalStorageService } from './local-storage/local-storage.service';
import { StorageController } from './storage.controller';

const kafkaURL = process.env.kafkaURL || 'localhost:9093';

@Module({
  imports: [
    PrismaModule,
    ClientsModule.register([
      {
        name: 'storageConsumer',
        transport: Transport.KAFKA,
        options: {
          client: {
            clientId: 'storage',
            brokers: [kafkaURL],
          },
          consumer: {
            groupId: 'storage-consumer-group',
          },
          producer: {},
          producerOnlyMode: true,
        },
      },
    ]),
  ],
  controllers: [StorageController],
  providers: [LocalStorageService],
})
export class StorageModule { }
