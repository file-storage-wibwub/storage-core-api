import {
  Inject,
  Injectable,
  OnModuleDestroy,
  OnModuleInit,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ClientKafka } from '@nestjs/microservices';
import { File } from '@prisma/client';
import * as bson from 'bson';
import * as fs from 'node:fs/promises';
import { PrismaService } from 'src/prisma/prisma.service';
import { CreateFileResponse } from '../dto/create-file.dto';

@Injectable()
export class LocalStorageService implements OnModuleInit, OnModuleDestroy {
  constructor(
    private readonly configService: ConfigService,
    private readonly prismaService: PrismaService,
    @Inject('storageConsumer') private readonly client: ClientKafka,
  ) { }

  async onModuleInit() {
    this.client.subscribeToResponseOf('create_metadata');
    await this.client.connect();
  }

  async onModuleDestroy() {
    await this.client.close();
  }

  async createFileObjectMetadata(objectID: string) {
    const data = await this.prismaService.file.create({
      data: {
        uploaderID: new bson.ObjectId().toString(), // Mock user id
        objectID: objectID,
      },
    });

    return data.uuid;
  }

  produceSaveFile(
    uuid: string,
    objectId: string,
    buffer: Buffer,
    type: string,
  ) {
    const storagePath = this.configService.get('storage_path', './storages');
    const filePath = storagePath + '/' + objectId;
    const oneYearLater = new Date();
    oneYearLater.setFullYear(new Date().getFullYear() + 1);

    return this.client.emit<CreateFileResponse>('create_metadata', {
      uuid: uuid,
      objectId: objectId,
      path: filePath,
      buffer,
      contentType: type,
      expiresDate: oneYearLater,
    });
  }

  async produceUpdateFile(objectId: string, buffer: Buffer, type: string) {
    return this.client.emit<CreateFileResponse>('update_file', {
      objectId: objectId,
      buffer,
      contentType: type,
    });
  }

  async produceDeleteFile(objectId: string) {
    return this.client.emit<CreateFileResponse>('delete_file', {
      objectId: objectId,
    });
  }

  async getObjectByObjectID(
    objectId: string,
  ): Promise<null | (File & { buffer?: Buffer })> {
    const file = await this.prismaService.file.findFirst({
      where: { objectID: objectId },
    });

    if (!file) {
      return null;
    }

    if (file.status !== 'waiting') {
      const buffer = await fs.readFile(file.path);

      return {
        ...file,
        buffer,
      };
    }

    return file;
  }
}
